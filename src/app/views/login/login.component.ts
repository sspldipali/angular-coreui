import { Component, OnInit } from "@angular/core";
import { LoginService } from "./login.service";
import { Storage } from "@ionic/storage-angular";
import { Router } from "@angular/router";

@Component({
  selector: "app-dashboard",
  templateUrl: "login.component.html",
})
export class LoginComponent implements OnInit {
  username = null;
  password = null;

  constructor(
    private loginService: LoginService,
    private storage: Storage,
    private router: Router
  ) {}

  async ngOnInit() {
    await this.storage.create();
  }

  login = () => {
    this.loginService
      .login({ username: this.username, password: this.password })
      .subscribe(
        (res) => {
          console.log("Login successfull!", res["data"].patient);
          this.storage.set("loggedInPatient", res["data"].patient);
          this.router.navigate(["search/doctors"]);
        },
        (err) => {
          console.log("Error: ", err.error.message);
        }
      );
  };

  createNewAccount(): void {
    this.router.navigate(["register"]);
  }
}
