import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class SearchDoctorService {
  constructor(private httpClient: HttpClient) {}

  getDoctors() {
    let url = environment.apiEndPoint + "/doctor";
    const httpHeader = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Accept: "application/json",
      }),
    };
    return this.httpClient.get(url, httpHeader);
  }

  bookAppointment(appointmentObj) {
    let url = `${environment.apiEndPoint}/doctor/appointment`;
    const httpHeader = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Accept: "application/json",
      }),
    };
    return this.httpClient.post(url, appointmentObj, httpHeader);
  }
}
