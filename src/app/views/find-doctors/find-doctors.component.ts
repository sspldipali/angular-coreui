import { Component, OnInit, ViewChild, Inject } from "@angular/core";
import { Router } from "@angular/router";
import { ModalDirective } from "ngx-bootstrap/modal";
import { SearchDoctorService } from "./find-doctors.service";
import { Storage } from "@ionic/storage-angular";
import * as moment from "moment";

// import { SESSION_STORAGE, WebStorageService } from "angular-webstorage-service";

@Component({
  selector: "app-find-doctors",
  templateUrl: "./find-doctors.component.html",
  styleUrls: ["./find-doctors.component.scss"],
})
export class FindDoctorsComponent implements OnInit {
  doctors = [];
  selectedDoctorForAppointment = null;
  date = null;
  time = null;
  loggedInPatient = null;

  @ViewChild("primaryModal") public primaryModal: ModalDirective;

  constructor(
    private router: Router,
    private searchDoctorService: SearchDoctorService,
    private storage: Storage // @Inject(SESSION_STORAGE) private storage: WebStorageService
  ) {}

  async ngOnInit() {
    await this.storage.create();
    this.loggedInPatient = await this.storage.get("loggedInPatient");

    this.searchDoctorService.getDoctors().subscribe(
      (res) => {
        console.log(res);
        this.doctors = res["data"].doctors;
      },
      (err) => {
        console.log("error", err.error.message);
      }
    );
  }

  openModel = (doctor) => {
    console.log("openModel :: doctorId: ", doctor);
    this.selectedDoctorForAppointment = doctor;
    this.primaryModal.show();
  };

  closeModal = () => {
    console.log("date: ", this.date);
    console.log("time: ", this.time);
    this.primaryModal.hide();
  };

  bookAppointment = () => {
    let appointmentObj = {
      patientId: this.loggedInPatient.id,
      doctorId: this.selectedDoctorForAppointment.id,
      patientName: `${this.loggedInPatient.firstName} ${this.loggedInPatient.lastName}`,
      appointmentDate: moment(`${this.date} ${this.time}`).valueOf(),
    };
    this.searchDoctorService.bookAppointment(appointmentObj).subscribe(
      (res) => {
        console.log(res);
        this.primaryModal.hide();
      },
      (err) => {
        console.log(err.error.message);
      }
    );
  };
}
