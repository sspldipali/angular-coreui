import { Component, OnInit } from "@angular/core";
import { DoctorLoginService } from "./doctorlogin.service";
import { Storage } from "@ionic/storage-angular";
import { Router } from "@angular/router";

@Component({
  selector: "app-doctorlogin",
  templateUrl: "./doctorlogin.component.html",
  styleUrls: ["./doctorlogin.component.scss"],
})
export class DoctorloginComponent implements OnInit {
  username = null;
  password = null;

  constructor(
    private doctorLoginService: DoctorLoginService,
    private storage: Storage,
    private router: Router
  ) {}

  async ngOnInit() {
    await this.storage.create();
  }

  login = () => {
    this.doctorLoginService
      .login({ username: this.username, password: this.password })
      .subscribe(
        (res) => {
          console.log("Login successfull!", res["data"].doctor);
          this.storage.set("loggedInDoctor", res["data"].doctor);
          this.router.navigate(["doc/appointments"]);
        },
        (err) => {
          console.log("Error: ", err.error.message);
        }
      );
  };
}
