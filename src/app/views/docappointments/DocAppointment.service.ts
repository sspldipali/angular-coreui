import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class DoctorAppointmentService {
  constructor(private httpClient: HttpClient) {}

  getAppointments(doctorId) {
    let url = environment.apiEndPoint + "/doctor/appointments/" + doctorId;
    const httpHeader = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Accept: "application/json",
      }),
    };
    return this.httpClient.get(url, httpHeader);
  }
}
