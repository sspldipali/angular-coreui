import { Component, OnInit } from "@angular/core";
import { DoctorAppointmentService } from "./DocAppointment.service";
import { Storage } from "@ionic/storage-angular";
import * as moment from "moment";

@Component({
  selector: "app-docappointments",
  templateUrl: "./docappointments.component.html",
  styleUrls: ["./docappointments.component.scss"],
})
export class DocappointmentsComponent implements OnInit {
  loggedInDoctor = null;
  appointments = [];

  constructor(
    private doctorAppointmentService: DoctorAppointmentService,
    private storage: Storage
  ) {}

  async ngOnInit() {
    await this.storage.create();
    this.loggedInDoctor = await this.storage.get("loggedInDoctor");

    this.doctorAppointmentService
      .getAppointments(this.loggedInDoctor.id)
      .subscribe(
        (res) => {
          console.log(res);
          this.appointments = res["data"].appointments.map((appointment) => {
            return {
              ...appointment,
              appointmentDate: moment(appointment.appointmentDate).format(
                "DD/MM/YYYY hh:mm a"
              ),
            };
          });
          console.log("this.appointments: ", this.appointments);
        },
        (err) => {
          console.log("error", err.error.message);
        }
      );
  }
}
