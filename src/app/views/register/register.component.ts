//import { Component } from '@angular/core';
//import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { RegisterService } from "./register.service";
import { Component, OnInit } from "@angular/core";
import { AlertConfig } from "ngx-bootstrap/alert";

export function getAlertConfig(): AlertConfig {
  return Object.assign(new AlertConfig(), { type: "success" });
}

@Component({
  selector: "app-dashboard",
  templateUrl: "register.component.html",
  providers: [{ provide: AlertConfig, useFactory: getAlertConfig }],
})
export class RegisterComponent implements OnInit {
  userName: string = "";
  email: string = null;
  password: string = null;
  firstName: string = null;
  lastName: string = null;
  isSuccess: boolean = false;
  successMessage: string = null;
  isError: boolean = false;
  errorMessage: string = null;

  constructor(
    private router: Router,
    private registerService: RegisterService
  ) {}
  ngOnInit(): void {
    //throw new Error('Method not implemented.');
    //this.checkOverflow();
    if (
      document.getElementById("userNameLoginField") != null &&
      document.getElementById("userNameLoginField") != undefined
    ) {
      document.getElementById("userNameSignUpField").focus();
    }
  }
  createProfile() {
    this.registerService
      .createProfile(this.firstName, this.lastName, this.email, this.password)
      .subscribe(
        (result) => {
          if (result) {
            console.log(result);
            this.isError = false;
            this.isSuccess = true;
            this.successMessage = result["message"];
            this.errorMessage = null;
            this.router.navigate(["login"]);
          }
        },
        (errorResponse) => {
          //this.dialogService.showDialog("ERROR","API");
          this.isError = true;
          this.isSuccess = false;
          this.successMessage = null;
          this.errorMessage = errorResponse.error.message;
          console.log("error", errorResponse.error.message);
          // this.errorMessage =
        }
      );
  }

  goBack(): void {
    this.router.navigate(["login"]);
  }
 
}
