import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class RegisterService {
  constructor(private httpClient: HttpClient) {}

  createProfile(firstName, lastName, email, password) {
    let url = environment.apiEndPoint + "/patient/register";
    const httpHeader = {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Accept: "application/json",
      }),
    };
    let body = {
      firstName,
      lastName,
      email,
      password,
    };
    return this.httpClient.post(url, body, httpHeader);
  }
}
