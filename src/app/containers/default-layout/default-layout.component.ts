import { Component, OnInit } from "@angular/core";
import { navItems } from "../../_nav";
import { Storage } from "@ionic/storage-angular";
import { Router } from "@angular/router";

@Component({
  selector: "app-dashboard",
  templateUrl: "./default-layout.component.html",
})
export class DefaultLayoutComponent implements OnInit {
  public sidebarMinimized = false;
  public navItems = navItems;

  constructor(private storage: Storage, private router: Router) {}

  async ngOnInit() {
    await this.storage.create();
  }

  toggleMinimize(e) {
    this.sidebarMinimized = e;
  }

  async logout() {
    // const sessionKeys = [...(await this.storage.keys())];
    // console.log("this.storage.keys(): ", sessionKeys);
    await this.storage.clear();
    this.router.navigate(["login"]);
  }
}
